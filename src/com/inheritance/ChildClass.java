package com.inheritance;

public class ChildClass extends ParentClass{
	public  void property2() {
		System.out.println("property2 is belongs to child");
	}
public static void main(String[] args) {
	ChildClass child=new ChildClass();
	ParentClass p=new ParentClass();
	child.property1();
	child.property2();
	p.property1();
	//p.property2();
}
}
