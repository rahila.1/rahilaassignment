package com.sample;

public class AbstractionTwo extends AbstractOne{
	
	public  void multiple() {
		System.out.println("multiple method");

	}
	public static void main(String[] args) {
		AbstractionTwo two=new AbstractionTwo();
		two.addMethod();
		two.subMethod();
		two.multiple();
	}
	@Override
	public void addMethod() {
		System.out.println("addition");
	}
	

}
