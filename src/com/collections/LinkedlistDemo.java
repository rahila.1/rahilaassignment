package com.collections;

import java.util.LinkedList;

public class LinkedlistDemo {
public static void main(String[] args) {
	LinkedList<String>linkedlist=new LinkedList<>();
	linkedlist.add("a");
	linkedlist.add("b");
	linkedlist.add("c");
	linkedlist.add("d");
	linkedlist.add("e");
	System.out.println(linkedlist);
	
	linkedlist.add(1, "a1");
	System.out.println(linkedlist);
	linkedlist.addFirst("z");
	linkedlist.add("y");
	System.out.println(linkedlist);
	linkedlist.remove("d");
	System.out.println(linkedlist);
}

}


