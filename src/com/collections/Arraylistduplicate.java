package com.collections;

import java.util.ArrayList;

public class Arraylistduplicate {
	public static void main(String[] args) {
		ArrayList<String>list=new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		System.out.println(list);
		list.add("b");    //dup value
		System.out.println(list);
		list.add(null);     //null value
		System.out.println(list);
	}
}
